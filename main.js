
/**
 *  Vue App
 */

let app = new Vue({
	el: '#exchange',
	data() {
		return {
			exg: "SEK",
			rates: {},
			calculationRates: {},
			currentRate: 0.114117363,
			selectedFromCurrency: "SEK",
			selectedToCurrency: "USD",
			inputAmount: 0,
			outputAmount: 0,
		}
	},

	/**
	 *  Virtual DOM is created but not yet mounted - get exhange rates based on property exg ("SEK")
	 *  @return {null} []
	 */
	created(){

		let scope = this;

		axios.get('https://api.exchangeratesapi.io/latest?base=' + this.exg)
		  .then(function (response) {
		    // handle success
		    scope.rates = response.data.rates;
		    scope.calculationRates = response.data.rates;
		  })
		  .catch(function (error) {
		    // handle error
		    console.log(error);
		  });
	},

	/**
	 *  Virtual DOM is created and mounted (ready to make changes in the DOM)
	 *  @return {null} []
	 */
	mounted() {
   
  	},

  	/**
  	 *  Object that stores methods to be called
  	 *  @type {Object}
  	 */
  	methods: {

  		/**
  		 *  Method that updates exchange rates based on user chosen currency
  		 *  @param  {event} e [event object]
  		 *  @return {null}   []
  		 */
  		changeExchangeRate(e) {

  			if(this.exg != e.target.id) {

  				this.exg = e.target.id;

	  			this.apiCall();
  			}
  		},

  		/**
  		 *  Updates user chosen "from" currency. The currency to be used when calculating rate to "to" currency
  		 *  @param  {event} e [event object]
  		 *  @return {null}   []
  		 */
  		updateFromCurrency(e) {

  			if (this.selectedFromCurrency != e.target.innerHTML) {

  				this.selectedFromCurrency = e.target.innerHTML;

	  			this.calculateExchangeRates(true);
  			}
  		},

  		/**
  		 *  Updates user chosen "to" currency. The currency to be used when calculating "from" currency
  		 *  @param  {event} e [event object]
  		 *  @return {null}   []
  		 */
  		updateToCurrency(e) {

  			if (this.selectedToCurrency != e.target.innerHTML) {

  				this.selectedToCurrency = e.target.innerHTML;

	  			this.calculateExchangeRates(false);
  			}

  		},

  		/**
  		 *  Updates inputAmount based on the user input in text field
  		 *  @param  {event} e [event object]
  		 *  @return {null}   []
  		 */
  		inputChanged(e) {

			this.inputAmount = e.target.value;

			this.calculateExchangeRates(false);

  		},

  		/**
  		 *  Calculates the exchange rate based on selectedFromCurrency, selectedToCurrency and inputAmount
  		 *  @return {null} []
  		 */
  		calculateExchangeRates(newAPICallNeeded) {
  			let scope = this;

  			if (newAPICallNeeded) {

  				axios.get('https://api.exchangeratesapi.io/latest?base=' + this.selectedFromCurrency)
				  .then(function (response) {
				    // handle success
				    scope.calculationRates = response.data.rates;
				    for (const key in scope.calculationRates) {
				    	if (key == scope.selectedToCurrency) {
				    		scope.currentRate = scope.calculationRates[key];
				    		scope.outputAmount = (parseInt(scope.inputAmount) * scope.currentRate);
				    	}
				    }
				  })
				  .catch(function (error) {
				    // handle error
				    console.log(error);
				  }
				);
  			}

  			else {

  				for (const key in this.calculationRates) {
  					if (key == this.selectedToCurrency) {
  						this.currentRate = this.calculationRates[key];
  					}
  				}

  				this.outputAmount = (parseInt(this.inputAmount) * this.currentRate);
  			}
  		},

  		/**
  		 *  Method for calling the API and updating the rates displayed to the user
  		 *  @return {null} []
  		 */
  		apiCall() {

  		let scope = this;

		axios.get('https://api.exchangeratesapi.io/latest?base=' + this.exg)
		  .then(function (response) {
		    // handle success
		    scope.rates = response.data.rates;
		  })
		  .catch(function (error) {
		    // handle error
		    console.log(error);
		  });
  		}
  	}
});
